module powertop

go 1.16

require (
	github.com/go-chi/chi v4.0.2+incompatible // indirect
	github.com/influxdata/influxdb-client-go/v2 v2.3.0 // indirect
	github.com/influxdata/telegraf v1.18.2 // indirect
)
