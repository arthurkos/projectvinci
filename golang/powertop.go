package main

import (
    "encoding/csv"
    "io"
    "log"
    "os"
    "github.com/influxdata/telegraf"
    "github.com/influxdata/telegraf/plugins/inputs"
    "github.com/influxdata/influxdb-client-go/v2"
)

type Powertop struct {
    Usage    string `csv:"Usage"`
    Events   string `csv:"Events"`
    Category string `csv:"Category"`
    Desc     string `csv:"Description"`
    PW       string `csv:"PW_Estimate"`
}

func (s *Powertop) Description() string {
    return "Gather Powertop infos"
}

func (s *Powertop) SampleConfig() string {
    return "Sample Config"
}

func (s *Powertop) Gather(acc telegraf.Accumulator) error {

    tags := map[string]string{}
    fields := map[string]interface{}{}

    csvFile, _ := os.Open("powertop.csv")
    reader := csv.NewReader(csvFile)
    defer csvFile.Close()

    for {

        itt, error := reader.Read()
        if error == io.EOF {
            break
        } else if error != nil {
            log.Fatal(error)
        }

        tags["Usage"] = itt[0]
        tags["Events"] = itt[1]
        tags["Category"] = itt[2]
        tags["Desc"] = itt[3]
        tags["PW"] = itt[4]

        acc.AddFields("powertop", fields, tags)
    }

    return nil
}

func main() {
    inputs.Add("powertop", func() telegraf.Input {
        return &Powertop{}
    })
  // You can generate a Token from the "Tokens Tab" in the UI
  const token = "LAVp9c5Xm7nnvTDpACvqA9bNAt6LWHrIrXTc4PQKueb-o7G5KwSacus6IPQoq4zHZEtmTqjhGUzEQAHVnT8DBQ=="
  const bucket = "supdevinci"
  const org = "supdevinci"

  client := influxdb2.NewClient("http://192.168.1.18:8086", token)
  // always close client at the end
  defer client.Close()
}

